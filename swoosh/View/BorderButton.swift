//
//  BorderButton.swift
//  swoosh
//
//  Created by Vignesh Kumar on 7/11/17.
//  Copyright © 2017 Vignesh Kumar. All rights reserved.
//

import UIKit

class BorderButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderWidth = 2.0
        layer.borderColor = UIColor.white.cgColor
    }

}
