//
//  Player.swift
//  swoosh
//
//  Created by Vignesh Kumar on 8/3/17.
//  Copyright © 2017 Vignesh Kumar. All rights reserved.
//

import Foundation

struct Player {
    var desiredLeague: String!
    var selectedSkillLevel: String!
}
